29/01/2018 :
----------

**Arduino ATMEGA2560**

Après quelques recherches cette board posséde [un port de programmation](https://github.com/RIOT-OS/RIOT/wiki/Board:-Arduino-Mega2560) (AVR) déjà disponible.
Je ne posséde pas le debugger adéquate pour cette cible SEGGER J-Trace for cortex M.

**STM32F051R8T6**

Cette carte posséde un port de debug SWD, ~~mon debugger ne supporte pas ce protocol~~ car mon debugger est obsoléte selon le fabricant. Mais elle posséde un JTAG !
Problème :

* Je n'arrive pas a me connecter à la carte avec le STLINK V2 on board.
* Le JTAG est apparent ce qui ne peut donc pas en faire une cible.

06/02/2018 :
------------

Suite à un conseil et pour mieux comprendre le protocol JTAG, je vais miantenant utiliser [OPENOCD](http://openocd.org/), si j'ai bien compris c'est un programme de configuration matériel faisant la liaison entre le debugger et l'ordinateur. Sa force c'est sa liberté et sa compatibilité car il est open-source et utilisable avec de nombreux debugger.

**STM32F303VCT6**
En utilisant un autre debugger [U-link pro](http://www2.keil.com/mdk5/ulink), j'ai enfin réussi à communiquer avec une carte en SWD ! 
Sur les différents essais que j'ai pu réaliser les différentes cibles que j'utilise possède une interface de programmation USB qui m'empeche de communiquer avec la puce... Probablement un conflit sur le SWDIO du port SWD. 
Hors sur cette carte il y a un port USER qui permet uniquement d'alimenter la carte ce qui me permet d'utiliser le port de debug sans conflit car le ST-link/V2 (debugger in board) n'est plus alimenté.

**Conclusion**,
Si je souhaite donc implémenter un jtag sur une carte qui en est dépourvu je dois supprimer la communication USB (Si il y en as une). De nombreuses carte possède la possibilté d'être alimenté extérieurement (A essayer).

09/02/2018 :
------------
Cible : **STM32F3 Discovery**

Mon objectif était donc de réussir à manipuler le processeur à l'aide de son port de debug (ici le SWD) sans utiliser les périphériques et autre système prévu à cet effet. 
Deux problèmes :
* Comme vu prédement, le port de debug doit être connecté à un seul périphérique pour éviter les conflits je dois donc isoler les pins.
* Ensuite, la méthode employé doit être applicable sur d'autres cibles car en effet ajouter un port de debug sur une carte qui en possède déjà un est relativement inutile mais c'est très formateur et riche en enseignement.

J'ai donc choisi de soulever les pins, dans notre cas le port SWD est très intéressant car il nous permet de soulever uniquement 2 pins (SWDIO et SWCLK). Les deux autres pins nécessaires comme le Vref et le reset peuvent être plus facilement trouvé sur n'importe qu'elle carte.

![IMG_0874](IMG_0874.JPG "IMG_0874.png")

La manipulation est assez simple, j'ai sollicité l'intervention d'une amie pour accélérer le processus et parce que c'est toujours  plus amusant de réaliser un projet à plusieurs.

![IMG_0876](IMG_0876.JPG "IMG_0876.png")

Voici donc le résultat final avec les 2 pins soulevés et deux fils soudés à ces mêmes pattes (Pin 72 SWDIO et Pin 76 SWCLK). Etant maintenant isolé de la carte : plus de conflit !

Pour le montage final rien de plus simple, j'utilise mon debugger avec ce port de debug "*custom*" ainsi que le Vref et le Reset.

![IMG_0875](IMG_0875.JPG "IMG_0875.png")

Mis en place d'**openOCD** et connexion avec le **J-link**, j'ai eu pas mal de soucis pour utiliser le programme principalement à cause d'un driver USB défaillant. Pour corriger celà j'ai installé un nouveau driver USB (Pour le debugger) à l'aide d'un petit logiciel lui aussi open-source nommé Zadig.

![Logiciel Zadig](zadig.jpg "Zadig.png")

J'ai ensuite décidé d'essayer de me connecter à la carte à l'aide du logiciel openOCD.

`> openocd -f ..\scripts\interface\jlink.cfg -f ..\scripts\board\summer.cfg`

Le fichier summer.cfg est l'équivalent de stm32f3discovery.cfg que j'ai modifié pour être utiliser avec mon debugger.

    # This is an STM32F3 discovery board with a single STM32F303VCT6 chip.
    # http://www.st.com/internet/evalboard/product/254044.jsp

    source [find interface/jlink.cfg]

    transport select swd

    source [find target/stm32f3x.cfg]

    reset_config srst_only

Vous avez surement remarqué cette ligne : `transport select swd` qui correspond à la sélection du protocol de communication à utiliser avec le debugger et j'ai en effet choisi le SWD. Pourtant, d'après le fabricant, ce debugger n'est pas compatible et surprise la communication fonctionne !

![Config](config.jpg "config.png")

Ensuite, il suffit d'ouvrir un client telnet comme *Putty* sur le port 4444 et voilà la magie opère. 

![Dumpcode](Dumpcode.jpg "Dumpcode.png")

Pour s'échauffer j'ai réalisé un dump du code. J'ai aussi à nouveau sollicité un ami pour essayer de comprendre ce dump et comparer au code original (en .hex), il est en effet identique mais nous sommes incapables de l'interprêter avec *IDA*. Nous sommes encore en pleine réflexion.

Pour finir est valider le challenge, il m'a suffit de charger un programme extrèmement compliqué comme celui-ci : `blinky.hex` dans notre board à l'aide de quelques commandes.

![FlashBlinky](FlashBlinky.jpg "FlashBlinky.png")

**Conclusion**, le système fonctionne et cette méthode est applicable à de nombreuses Boards. Même en ayant perdu de l'attrait car la cible ne présente que peu d'intéret, j'ai beaucoup appris sur les différents port de debug ainsi que sur leurs méthodes de fonctionnement de plus j'ai découvert divers logiciels peu accueillant mais drôlement bien fichu. 

B-Max